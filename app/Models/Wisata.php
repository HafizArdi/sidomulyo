<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Wisata as Authenticatable;
use Illuminate\Support\Facades\Hash;

class Wisata extends Model{

    protected $fillable = [
        'foto','namaWisata', 'deskripsi',
        
    ];
   
}
