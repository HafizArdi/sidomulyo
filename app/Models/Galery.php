<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Galery as Authenticatable;
use Illuminate\Support\Facades\Hash;

class Galery extends Model{
    protected $fillable = [
        'foto','tanggal','namaKegiatan', 'deskripsi',
        
    ];
}
