<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Wirausaha as Authenticatable;
use Illuminate\Support\Facades\Hash;

class Wirausaha extends Model{

    protected $fillable = [
        'foto','namaWirausaha', 'deskripsi',
        
    ];
}
