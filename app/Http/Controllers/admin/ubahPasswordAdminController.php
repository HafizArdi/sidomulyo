<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Repositories\UserRepository;
use Hash;


class ubahPasswordAdminController extends Controller{

    private $UserRepository;

    public function __construct(UserRepository $UserRepository){

        $this->UserRepository = $UserRepository;

    }
   
    public function index(Request $request){
        // $page = $request->get('page', 1);
        // $limit = 10;
        $user = $this->UserRepository->get(null);
        $data = [
            'user' => $user
        ];
        return view('admin/user/index',$data);
    }

    public function update(Request $request, $id){
        $detail = User::where('id', $id)->first();

        $validate = [
            'name' => 'required|max:191',
            'email' => 'required|max:191',
            'password' => 'required|max:191',
          
        ];
        $this->validate(request(), $validate);
        $detail->name = $request->get('name');
        $detail->email = $request->get('email');
        $detail->password = Hash::make($request->get('password'));
        $detail->save();
        \Session::flash('notif-success', 'Update data berhasil');
        return redirect()->route('ubahPasswordAdmin.index')->with('success', 'Successfully created.');



    }


}
