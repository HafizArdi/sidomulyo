<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Wirausaha;
use App\Repositories\WirausahaRepository;
use File;


class wirausahaController extends Controller{

    private $WirausahaRepository;

    public function __construct(WirausahaRepository $WirausahaRepository){

        $this->WirausahaRepository = $WirausahaRepository;

    }
   
    public function index(Request $request){

        $page = $request->get('page', 1);
        $limit = 10;

        $wirausaha = $this->WirausahaRepository->get($limit);
        $data = [
            'no' => ($page - 1) * $limit,
            'wirausaha' => $wirausaha
        ];
        return view('admin/wirausaha/index',$data);

    }

    public function store(Request $request){

        $validate = [
            'foto' => 'required|image|mimes:jpeg,png,jpg',
            'namaWirausaha' => 'required|max:100',
            'deskripsi' => 'required|max:255',
          
        ];
        
        $this->validate(request(), $validate);
        $image = $request->file('foto');
        $imageName = $image->getClientOriginalName();
        $image->move(public_path('assets/images/wirausaha'), $imageName);
        $detail = new Wirausaha();
        $detail->namaWirausaha = $request->get('namaWirausaha');
        $detail->deskripsi = $request->get('deskripsi');
        $detail->foto = $imageName;
        $detail->save();
        return redirect()->route('wirausahaAdmin.index')->with('success', 'Successfully created.');

    }

    public function update(Request $request, $id){
        $detail = Wirausaha::where('id', $id)->first();
        
        $validate = [
            'foto' => 'required|image|mimes:jpeg,png,jpg',
            'namaWirausaha' => 'required|max:255',
            'deskripsi' => 'required|max:255',
          
        ];

        $image = $request->file('foto');

        if($image == null){
            $imageName = $detail->foto;
            
        }else {
            $image_path = public_path('assets/images/wirausaha/'.$detail->foto);
            if (File::exists($image_path)) {
                unlink($image_path);
            }
            $imageName = $image->getClientOriginalName();
            $image->move(public_path('assets/images/wirausaha'), $imageName);
        }

        $detail->namaWirausaha = $request->get('namaWirausaha');
        $detail->deskripsi = $request->get('deskripsi');
        $detail->foto = $imageName;
        $detail->save();
        \Session::flash('notif-success', 'Update data berhasil');
        return redirect()->route('wirausahaAdmin.index')->with('success', 'Successfully created.');

    }

    public function delete($id){
        $detail = Wirausaha::where('id', $id)->first();
        $image_path = public_path('assets/images/wirausaha/'.$detail->foto);

        if(!$detail){
            \Session::flash('notif-error', 'Data tidak ditemukan');
            return redirect(route('wirausahaAdmin.index'));
        }

        if (File::exists($image_path)) {
            unlink($image_path);
        }

        $detail->delete();

        \Session::flash('notif-success', 'Data berhasil dihapus');
        return redirect()->route('wirausahaAdmin.index')->with('success', 'Designations successfully deleted.');
    }
}
