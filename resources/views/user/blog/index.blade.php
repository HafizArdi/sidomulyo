@extends('user.layouts.app')
@section('content')
<section class="hero-wrap js-fullheight" style="background-image: url('{{asset('assets/images/bg_2.jpg')}}');">
    <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center pt-5 mt-md-5">
                    <h1 class="mb-3 bread">Our Blog</h1>
                    <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span>Blog</span></p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="ftco-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 ftco-animate">
			    <div class="row">
				    <div class="col-md-6 d-flex ftco-animate">
		          	    <div class="blog-entry justify-content-end">
		                    <a href="blog-single.html" class="block-20" style="background-image: url('{{asset('assets/images/image_1.jpg')}}');"></a>
		                    <div class="text p-4 float-right d-block">
		              	        <div class="meta">
		                            <div><a href="#">July 01, 2019</a></div>
		                            <div><a href="#">Admin</a></div>
		                            <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
		                        </div>
		                        <h3 class="heading mt-2"><a href="#">Young Women Doing Abdominal</a></h3>
		                        <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
		                    </div>
		                </div>
		            </div>
		            <div class="col-md-6 d-flex ftco-animate">
		          	    <div class="blog-entry justify-content-end">
		                    <a href="blog-single.html" class="block-20" style="background-image: url('{{asset('assets/images/image_2.jpg')}}');"></a>
		                    <div class="text p-4 float-right d-block">
		              	        <div class="meta">
		                            <div><a href="#">July 01, 2019</a></div>
		                            <div><a href="#">Admin</a></div>
		                            <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
		                        </div>
		                        <h3 class="heading mt-2"><a href="#">Young Women Doing Abdominal</a></h3>
		                        <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
		                    </div>
		                </div>
		            </div>
		            <div class="col-md-6 d-flex ftco-animate">
		          	    <div class="blog-entry">
		                    <a href="blog-single.html" class="block-20" style="background-image: url('{{asset('assets/images/image_3.jpg')}}');"></a>
		                    <div class="text p-4 float-right d-block">
		              	        <div class="meta">
		                            <div><a href="#">July 01, 2019</a></div>
		                            <div><a href="#">Admin</a></div>
		                            <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
		                        </div>
		                        <h3 class="heading mt-2"><a href="#">Young Women Doing Abdominal</a></h3>
		                        <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
		                    </div>
		                </div>
		            </div>
		            <div class="col-md-6 d-flex ftco-animate">
		          	    <div class="blog-entry">
		                    <a href="blog-single.html" class="block-20" style="background-image: url('{{asset('assets/images/image_4.jpg')}}');"></a>
		                    <div class="text p-4 float-right d-block">
		              	        <div class="meta">
		                            <div><a href="#">July 01, 2019</a></div>
		                            <div><a href="#">Admin</a></div>
		                            <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
		                        </div>
		                        <h3 class="heading mt-2"><a href="#">Young Women Doing Abdominal</a></h3>
		                        <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
		                    </div>
		                </div>
		            </div>
		            <div class="col-md-6 d-flex ftco-animate">
		          	    <div class="blog-entry">
		                    <a href="blog-single.html" class="block-20" style="background-image: url('{{asset('assets/images/image_5.jpg')}}');"></a>
		                    <div class="text p-4 float-right d-block">
		              	        <div class="meta">
		                            <div><a href="#">July 01, 2019</a></div>
		                            <div><a href="#">Admin</a></div>
		                            <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
		                        </div>
		                        <h3 class="heading mt-2"><a href="#">Young Women Doing Abdominal</a></h3>
		                        <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
		                    </div>
		                </div>
		            </div>
		            <div class="col-md-6 d-flex ftco-animate">
		          	    <div class="blog-entry">
		                    <a href="blog-single.html" class="block-20" style="background-image: url('{{asset('assets/images/image_6.jpg')}}');"></a>
		                    <div class="text p-4 float-right d-block">
		              	        <div class="meta">
		                            <div><a href="#">July 01, 2019</a></div>
		                            <div><a href="#">Admin</a></div>
		                            <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
		                        </div>
		                        <h3 class="heading mt-2"><a href="#">Young Women Doing Abdominal</a></h3>
		                        <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
		                    </div>
		                </div>
		            </div>
				</div>
				<div class="row mt-5">
		            <div class="col text-center">
		                <div class="block-27">
		                    <ul>
		                        <li><a href="#">&lt;</a></li>
		                        <li class="active"><span>1</span></li>
		                        <li><a href="#">2</a></li>
		                        <li><a href="#">3</a></li>
		                        <li><a href="#">4</a></li>
		                        <li><a href="#">5</a></li>
		                        <li><a href="#">&gt;</a></li>
		                    </ul>
		                </div>
		            </div>
		        </div>
          </div> 
          <div class="col-lg-4 sidebar ftco-animate">
                <div class="sidebar-box">
                    <form action="#" class="search-form">
                        <div class="form-group">
                	        <div class="icon">
	                            <span class="icon-search"></span>
	                        </div>
                            <input type="text" class="form-control" placeholder="Type a keyword and hit enter">
                        </div>
                    </form>
                </div>
                <div class="sidebar-box ftco-animate">
                    <div class="categories">
                        <h3 class="heading-2">Categories</h3>
                        <li><a href="#">Workout <span>(12)</span></a></li>
                        <li><a href="#">Gym <span>(22)</span></a></li>
                        <li><a href="#">Crossfit <span>(37)</span></a></li>
                        <li><a href="#">Body Fit <span>(42)</span></a></li>
                        <li><a href="#">Fitness <span>(14)</span></a></li>
                        <li><a href="#">Yoga <span>(140)</span></a></li>
                    </div>
                </div>
                <div class="sidebar-box ftco-animate">
                    <h3 class="heading-2">Recent Blog</h3>
                    <div class="block-21 mb-4 d-flex">
                        <a class="blog-img mr-4" style="background-image: url({{asset('assets/images/image_1.jpg')}});"></a>
                        <div class="text">
                            <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the blind texts</a></h3>
                            <div class="meta">
                                <div><a href="#"><span class="icon-calendar"></span> July 01, 2019</a></div>
                                <div><a href="#"><span class="icon-person"></span> Admin</a></div>
                                <div><a href="#"><span class="icon-chat"></span> 19</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="block-21 mb-4 d-flex">
                        <a class="blog-img mr-4" style="background-image: url({{asset('assets/images/image_2.jpg')}});"></a>
                        <div class="text">
                            <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the blind texts</a></h3>
                            <div class="meta">
                                <div><a href="#"><span class="icon-calendar"></span> July 01, 2019</a></div>
                                <div><a href="#"><span class="icon-person"></span> Admin</a></div>
                                <div><a href="#"><span class="icon-chat"></span> 19</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="block-21 mb-4 d-flex">
                        <a class="blog-img mr-4" style="background-image: url({{asset('assets/images/image_3.jpg')}});"></a>
                        <div class="text">
                            <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the blind texts</a></h3>
                            <div class="meta">
                                <div><a href="#"><span class="icon-calendar"></span> July 01, 2019</a></div>
                                <div><a href="#"><span class="icon-person"></span> Admin</a></div>
                                <div><a href="#"><span class="icon-chat"></span> 19</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sidebar-box ftco-animate">
                    <h3 class="heading-2">Tag Cloud</h3>
                    <div class="tagcloud">
                        <a href="#" class="tag-cloud-link">dish</a>
                        <a href="#" class="tag-cloud-link">menu</a>
                        <a href="#" class="tag-cloud-link">food</a>
                        <a href="#" class="tag-cloud-link">sweet</a>
                        <a href="#" class="tag-cloud-link">tasty</a>
                        <a href="#" class="tag-cloud-link">delicious</a>
                        <a href="#" class="tag-cloud-link">desserts</a>
                        <a href="#" class="tag-cloud-link">drinks</a>
                    </div>
                </div>
                <div class="sidebar-box ftco-animate">
                    <h3 class="heading-2">Paragraph</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus itaque, autem necessitatibus voluptate quod mollitia delectus aut, sunt placeat nam vero culpa sapiente consectetur similique, inventore eos fugit cupiditate numquam!</p>
                </div>
            </div>
        </div>
    </div>
</section>  
@endsection