@extends('user.layouts.app')
@section('content')
<section class="hero-wrap js-fullheight" style="background-image: url('{{asset('assets/images/wisata/pemandangan.JPG')}}');">
    <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center mt-md-5 pt-5">
                    <h1 class="mb-3 bread">Wisata</h1>
                    <p class="breadcrumbs"><span class="mr-2"><a href="{{route('user.home')}}">Home</a></span> <span>Wisata</span></p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="ftco-section bg-light">
    <div class="container-fluid px-4">
    	<div class="row">
		@foreach ($wisata as $key => $wisataa)
    	    <div class="col-lg-3 d-flex">
    	        <div class="coach align-items-stretch">
					<a href="#" data-toggle="modal" data-target="#exampleModal{{ $wisataa->id }}" class="img d-flex justify-content-center align-items-center" style="background-image: url({{asset('assets/images/wisata/'.$wisataa->foto)}});"></a>
	    			<div class="text pt-3 ftco-animate">
	    			    <h3><a href="#">{{ $wisataa->namaWisata }}</a></h3>
	    				<p>{{ $wisataa->deskripsi }}</p>
	    			</div>
	    		</div>
            </div>
			<div class="modal fade" id="exampleModal{{ $wisataa->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            	<div class="modal-dialog" role="document">
                	<div class="modal-content">
						<div class="col-md-55">
                            <div class="thumbnail">
                                <div class="image view view-first">
                                	<img style="width: 100%; display: block;" src="{{asset('assets/images/wisata/'.$wisataa->foto)}}" alt="image" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		@endforeach
        </div>
	{{ $wisata->links() }}
    </div>
</section>
@endsection