@extends('user.layouts.app')
@section('content')
<section class="hero-wrap js-fullheight" style="background-image: url('{{asset('assets/images/galery/test.JPG')}}');">
	<div class="overlay"></div>
		<div class="container">
        	<div class="row no-gutters slider-text js-fullheight align-items-center justify-content-center">
            	<div class="col-md-9 ftco-animate text-center pt-5 mt-md-5">
                	<h1 class="mb-3 bread">Galery</h1>
                	<p class="breadcrumbs"><span class="mr-2"><a href="{{route('user.home')}}">Home</a></span> <span>Galery</span></p>
                </div>
            </div>
		</div>
	</div>
</section>
<section class="ftco-section">
	<div class="container-fluid px-4">
    	<div class="row">
		@foreach ($galery as $key => $galeryy)
			<div class="col-md-6 col-lg-3">
        		<div class="package-program ftco-animate">
        			<a href="#" data-toggle="modal" data-target="#exampleModal{{ $galeryy->id }}" class="img d-flex justify-content-center align-items-center" style="background-image: url({{asset('assets/images/galery/'.$galeryy->foto)}});"></a>
        			<div class="text mt-3">
        				<h3><a href="#">{{ $galeryy->namaKegiatan }}</a></h3>
        				<p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
        			</div>
        		</div>
			</div>
			<div class="modal fade" id="exampleModal{{ $galeryy->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            	<div class="modal-dialog" role="document">
                	<div class="modal-content">
						<div class="col-md-55">
                            <div class="thumbnail">
                                <div class="image view view-first">
                                	<img style="width: 100%; display: block;" src="{{asset('assets/images/galery/'.$galeryy->foto)}}" alt="image" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		@endforeach
    </div>
{{ $galery->links() }}
</div>
</section>
@endsection